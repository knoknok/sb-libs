#include "push_button.h"
#include "application.h"

PushButton::PushButton(int pin) {
    _pin = pin;
    _state = HIGH;
}

void PushButton::init() {
    pinMode(_pin, INPUT_PULLUP);
}

bool PushButton::checkPressed(void) {
    int     readState = digitalRead(_pin);
    bool    pressed = false;

    if ((_state == HIGH) and (readState == LOW)) {
        pressed = true;
    }
    _state = readState;

    return pressed;
}
