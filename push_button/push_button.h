#ifndef _PUSH_BUTTON_H
#define _PUSH_BUTTON_H

class PushButton {
    private:
        int _pin;
        int _state;
    public:
        PushButton(int pin);
        void init(void);
        bool checkPressed(void);
};

#endif
