#ifndef _PROXIMITY_SENSOR_H
#define _PROXIMITY_SENSOR_H

class ProximitySensor {
    private:
        int     _minDistance, _maxDistance;
        int     _echoPin, _trigPin;
        bool    _state;

    public:
        ProximitySensor(int minDistance, int maxDistance, int echoPin, int trigPin);
        void init(void);
        bool proximityCheck(void);
};

#endif
