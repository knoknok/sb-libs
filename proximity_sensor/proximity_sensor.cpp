#include "proximity_sensor.h"
#include "application.h"

ProximitySensor::ProximitySensor(int minDistance, int maxDistance, int echoPin, int trigPin) {
    _minDistance = minDistance;
    _maxDistance = maxDistance;
    _echoPin = echoPin;
    _trigPin = trigPin;
    _state = false;
}

void ProximitySensor::init(void) {
    pinMode(_trigPin, OUTPUT);
    digitalWriteFast(_trigPin, LOW);
    pinMode(_echoPin, INPUT);
}

bool ProximitySensor::proximityCheck(void) {
    long    duration;
    float   distance;
    bool    change = false;

    digitalWrite(_trigPin, LOW);
    delayMicroseconds(2);

    digitalWriteFast(_trigPin, HIGH);
    delayMicroseconds(10);
    digitalWriteFast(_trigPin, LOW);

    duration = pulseIn(_echoPin,HIGH);
    distance = duration / 58.0; // Formula from datasheet

    if ((distance >= _minDistance) and (distance <= _maxDistance)) {
        if (!_state) {
            _state = true;
            change = true;
        }
    } else {
        _state = false;
    }

    return change;
}
