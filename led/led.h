#ifndef _LED_H_
#define _LED_H_

class Led {
    private:
        int _pin;

    public:
        Led(int pin);
        void init(void);
        void on(void);
        void off(void);
};

#endif
