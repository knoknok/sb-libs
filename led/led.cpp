#include "led.h"
#include "application.h"

Led::Led(int pin) {
    _pin = pin;
}

void Led::init(void) {
    pinMode(_pin, OUTPUT);
}

void Led::on(void) {
    digitalWrite(_pin, HIGH);
}

void Led::off(void) {
    digitalWrite(_pin, LOW);
}
