/*
The implementation of the class has to be done in the .h file because of the
use of templates.
*/

#ifndef _WEBHOOK_
#define _WEBHOOK_

#include <map>
#include "application.h"

typedef std::map<String, String> WhVar;

class Webhook {
    public:
        static bool subscribe(String eventName, EventHandler handler) {
            return Particle.subscribe("hook-response/" + eventName, handler, MY_DEVICES);
        }

        template <class T>
        static bool subscribe(String eventName, void (T::*handler)(const char *, const char *), T *instance) {
            return Particle.subscribe("hook-response/" + eventName, handler, instance, MY_DEVICES);
        }

        static void publish(String eventName, WhVar vars) {
            WhVar::iterator it;
            String          data("");

            for(it = vars.begin(); it != vars.end(); it++) {
                data += "\"" + it->first + "\":\"" + it->second + "\",";
            }
            data.remove(data.lastIndexOf(','));
            data = "{" + data + "}";

            Particle.publish(eventName, data, 60, PRIVATE);
        }
};

#endif
