#ifndef _OLED_DISPLAY_H
#define _OLED_DISPLAY_H

#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"

class OledDisplay_ {
    public:
        Adafruit_SSD1306    *display;
        void                init(int dcPin, int csPin, int resetPin);
        void                autoOff(void);
};

class OledDisplay {
    private:
        int             _dcPin, _csPin, _resetPin;
        int             _autoOffDelay;
        OledDisplay_    _display;
        Timer           *_autoOffTimer;
    public:
        OledDisplay(int dcPin, int csPin, int resetPin, int autoOffDelay);
        void    init(void);
        void    print(String s, bool autoOff);
        void    stopAutoOffTimer(void);
};

#endif
