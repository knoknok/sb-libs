#include "oled_display.h"

/*
** OledDisplay_ - Helper class
*/
void OledDisplay_::init(int dcPin, int csPin, int resetPin) {
    display = new Adafruit_SSD1306(dcPin, resetPin, csPin);
    display->begin(SSD1306_SWITCHCAPVCC);
    display->display();
    delay(2000);
    display->clearDisplay();
    display->display();
}

void OledDisplay_::autoOff(void) {
    display->clearDisplay();
    display->display();
}


/*
** OledDisplay - Main class
*/
OledDisplay::OledDisplay(int dcPin, int csPin, int resetPin, int autoOffDelay) {
    _dcPin = dcPin;
    _csPin = csPin;
    _resetPin = resetPin;
    _autoOffDelay = autoOffDelay;
}

void OledDisplay::init(void) {
    _display.init(_dcPin, _csPin, _resetPin);
    _autoOffTimer = new Timer(_autoOffDelay, &OledDisplay_::autoOff, _display);
}

void OledDisplay::print(String s, bool autoOff) {
    _display.display->setTextSize(2);
    _display.display->setTextColor(WHITE);
    _display.display->setCursor(10,0);
    _display.display->clearDisplay();
    _display.display->println(s);
    _display.display->display();

    if (autoOff == true) {
        _autoOffTimer->start();
    }
}

void OledDisplay::stopAutoOffTimer(void) {
    _autoOffTimer->stop();
}
