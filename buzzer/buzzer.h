#ifndef _BUZZER_H_
#define _BUZZER_H_

#include "application.h"

#define BUZZ_DURATION   5000 // ms

class Buzzer {
    private:
        int     _pin;
        Timer   *_timer;

    public:
        Buzzer(int pin) {
            _pin = pin;
        }

        template <class T>
        void init(void (T::*handler)(), T &instance) {
            pinMode(_pin, OUTPUT);
            _timer = new Timer(BUZZ_DURATION, handler, instance, true);
        }

        void startBuzz(int frequency, int duration) {
            _timer->start();
            tone(_pin, frequency, duration);
        }

        void stopBuzz(void) {
            _timer->stop();
            noTone(_pin);
        }
};

#endif
