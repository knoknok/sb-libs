#include "sb_data.h"

void SbData::populate(const char *data) {
    char    *args = strdup(data);

    // Respect order of fields in model (see B&)
    id = strtok(args, "|");
    name = strtok(NULL, "|");
    away = (strcmp(strtok(NULL, "|"), "true") == 0) ? true : false;
    welcome_message = strtok(NULL, "|");
    coming_message = strtok(NULL, "|");
    come_up_message = strtok(NULL, "|");
    no_answer_message = strtok(NULL, "|");
    particle_display_id = strtok(NULL, "|");
    particle_bell_id = strtok(NULL, "|");
    avatar = strtok(NULL, "|");
    ringing = (strcmp(strtok(NULL, "|"), "true") == 0) ? true : false;
    particle_access_token = strtok(NULL, "|");
    last_ring_on = strtok(NULL, "|");
}
