#ifndef _SB_DATA_H_
#define _SB_DATA_H_

#include "application.h"

class SbData {
    public:
        String  id;
        String  name;
        bool    away;
        String  welcome_message;
        String  coming_message;
        String  come_up_message;
        String  no_answer_message;
        String  particle_bell_id;
        String  particle_display_id;
        String  particle_access_token;
        String  avatar;
        bool    ringing;
        String  last_ring_on;

        void populate(const char *data);
};

#endif
